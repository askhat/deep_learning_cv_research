import os
import math
import numpy as np
import keras
import shutil
import matplotlib.pyplot as plt
import matplotlib.pyplot as mpl

from keras.models import Sequential
from keras.layers import BatchNormalization
from keras.layers import LeakyReLU
from keras.layers import Dropout
from my_classes import DataGenerator
from keras.layers import Dense
from keras import backend as K



#Datasets
PATH = "image_net_transfer_learning_features_VGG_EMBEDDING"
dataPath = "last_data"
querySize = 200
numberOfClasses = 5


queries = list()
isQuery = dict()
partition = dict()
partition['train'] = list()
partition['validation'] = list()
labels = {}
params = {
          'dim': querySize * numberOfClasses,
          'batch_size': 128,
          'n_classes': numberOfClasses,
          'shuffle': True,
          'queries': list()
}

architectureOfNN = [100, 70, 40, 5]
adamOptimizer = 0.001
allEpochs = 400
trainSplitSize = 0.80


#env
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
K.tensorflow_backend._get_available_gpus()


#data rename and move functions
def getAllDirsAndFiles(dir):
    files = []
    for filename in os.listdir(dir):
        files.append(filename)
    return files


def getAllFileNames(dir):
    files = []
    for filename in os.listdir(dir):
        if ".txt" in filename:
            files.append(filename)
    return files


def renameFiles(path, label, labelName):
    files = getAllFileNames(path)
    counter = 1
    for file in files:
        if ".txt" in file:
            print("Renaming " + file)
            os.rename(path + "/" + file, path + "/" + str(label) + "_" + labelName + "_" + str(counter) + ".txt")
            counter += 1


def prepareData(path):
    dirs = getAllDirsAndFiles(path)
    print(dirs)
    label = 0
    for dir in dirs:
        renameFiles(path + "/" + dir, label, dir)
        label += 1

    print("RENAMED!!!")
    for dir in dirs:
        files = getAllFileNames(path + "/" + dir)
        for file in files:
            filePath = path + "/" + dir + "/" + file
            shutil.copy(filePath, dataPath + "/" + file)
            print(filePath + " moved!")
#end of data rename and move functions


def getQueries(path):
    global querySize
    files = getAllFileNames(path)
    print("IN TOTAL " + str(len(files)))
    files.sort()
    index = 0
    while index < len(files):
        label = getLabel(files[index])
        counter = 0
        while index < len(files) and getLabel(files[index]) == label:
            if counter < querySize:
                queries.append(files[index])
                counter += 1
            index += 1

    print("SHOWING QUERIES:")
    for query in queries:
        isQuery[query] = True
        #filling params
        params['queries'].append(query)
        print(query)


def getLabel(imgName):
    return int(imgName[0:imgName.find('_')])

def getPartitionAndLabels(path, divide):
    files = getAllFileNames(path)
    files.sort()
    index = 0

    while index < len(files):
        label = getLabel(files[index])
        imagesOfOneClass = []
        while index < len(files) and getLabel(files[index]) == label:
            if files[index] not in queries:
                imagesOfOneClass.append(files[index])
            #imagesOfOneClass.append(files[index])
            index += 1

        print("Size of imagesOfOneClass " + str(len(imagesOfOneClass)))
        #filling train
        trainSize = int(divide * len(imagesOfOneClass))
        for i in range(0, trainSize):
            partition['train'].append(imagesOfOneClass[i])

        #filling validation
        for i in range(trainSize, len(imagesOfOneClass)):
            if imagesOfOneClass[i] not in queries:
                partition['validation'].append((imagesOfOneClass[i]))

        #filling labels
        for i in range(0, len(imagesOfOneClass)):
            labels[imagesOfOneClass[i]] = label



def baseline_model():
    # create model
    model = Sequential()
    model.add(Dense(architectureOfNN[0], input_dim=params['dim'], activation='relu'))
    model.add(BatchNormalization())

    model.add(Dense(architectureOfNN[1]))
    model.add(LeakyReLU(alpha=0.05))
    model.add(Dropout(0.40))
    model.add(BatchNormalization())

    model.add(Dense(architectureOfNN[2]))
    model.add(LeakyReLU(alpha=0.05))
    model.add(Dropout(0.45))
    model.add(BatchNormalization())

    #model.add(Dense(architectureOfNN[3]))
    #model.add(LeakyReLU(alpha=0.05))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.60))
    
    #model.add(Dense(architectureOfNN[4]))
    #model.add(LeakyReLU(alpha=0.05))
    #model.add(BatchNormalization())
    #model.add(Dropout(0.60))    

    model.add(Dense(params['n_classes'], activation='softmax'))

    # Compile model
    adam = keras.optimizers.Adam(lr=adamOptimizer)
    model.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy'])
    return model

#util functions
def getListOfFeatures(path):
    f = open(path)
    res = []
    for line in f:
        line = line[1:len(line)-2]
        curList = [float(item) for item in line.split(',')]
        res.append(curList)
    return np.array(res).flatten()

def writeToAFile(name, curList):
    f = open(name + ".txt", "w")
    for item in curList:
        f.write(item)
        f.write(os.linesep)

#must be in the same order
#prepareData(PATH)
#exit()

getQueries(dataPath)
getPartitionAndLabels(dataPath, trainSplitSize)

writeToAFile("queries", params['queries'])
writeToAFile("rest", partition['train'] + partition['validation'])


model = baseline_model()
training_generator = DataGenerator(partition['train'], labels, **params)
validation_generator = DataGenerator(partition['validation'], labels, **params)

history = model.fit_generator(generator=training_generator,
                    validation_data=validation_generator,
                    use_multiprocessing=True, epochs=allEpochs, verbose=1)

# evaluate the model
# _, train_acc = model.evaluate(x_train, y_train, verbose=0)
# _, test_acc = model.evaluate(x_test, y_test, verbose=0)
# print('Train: %.3f, Test: %.3f' % (train_acc, test_acc))



mpl.rcParams['font.size'] = 8

mainTitle = '300x300, Architecture=' + str(architectureOfNN) + ",\nadam=" + str(adamOptimizer) + ", epochs=" + str(allEpochs) + ", batch=" + str(params['batch_size'])  + ",\ntrain=" + str(trainSplitSize) + ", queryPerSize=" + str(querySize) + ", trainingSample=" + str(len(partition['train'])) + ", validate=" + str(len(partition['validation']))


print(history.history.keys())
# summarize history for accuracy
#plt = plt.figure(figsize=(60,60))
plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy ' + mainTitle)
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss ' + mainTitle)
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
