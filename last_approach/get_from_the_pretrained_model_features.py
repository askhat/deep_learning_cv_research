import os
from keras.layers import *
from keras.models import Model
from keras.preprocessing import image
from keras.applications import vgg16
from keras.applications.vgg16 import preprocess_input
from keras.models import load_model


import os
import numpy as np


model = load_model('askhat_without_6_layers.h5')


def getfeaturesOfMyVGGG(img_path):
    img = image.load_img(img_path, target_size=(224, 224))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    features = model.predict(x)

    model_extractfeatures = Model(input=model.input, output=model.get_layer('dense_2').output)
    fc2_features = model_extractfeatures.predict(x)
    fc2_features = fc2_features.reshape((300,1))

    featuresList = []
    for cur in fc2_features:
        featuresList.append(cur[0])
    featuresList.sort(reverse=True)

    return featuresList



def getAllFileNames(dir):
    files = []
    for filename in os.listdir(dir):
        files.append(filename)
    files.sort()
    return files

def writeToFile(path, featureList):
    f = open(path + ".txt", "w")
    for i in range(0, len(featureList)):
        f.write(str(featureList[i]) + " ")
    f.write(os.linesep)
    f.close()


#configs
mainPath = "image_net_big_sample_images"
featuresPath = "image_net_transfer_learning_features_VGG"

total = 8500

dirs = getAllFileNames(mainPath)
for dir in dirs:
    os.mkdir(featuresPath + "/" + dir)
    images = getAllFileNames(mainPath + "/" + dir)
    print("Creating dir " + dir)
    counter = 0
    
    for img in images:
        counter += 1
        print("getting features from " + mainPath + "/" + dir + "/" + img + " " + str(counter) + " out of " + str(total))
        features = getfeaturesOfMyVGGG(mainPath + "/" + dir + "/" + img)
        print("Feature size is " + str(len(features)) + " for " + img)
        writeToFile(featuresPath + "/" + dir + "/" + img, features)
    print("DONE with " + dir + " " + str(counter) + " images extracted...")





