from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.multiclass import OneVsRestClassifier
from sklearn.ensemble import BaggingClassifier, RandomForestClassifier
from collections import defaultdict
from sklearn.svm import SVC
from sklearn import metrics
from sklearn import datasets
from sklearn import svm
from sklearn import metrics
import os
import numpy as np
import time


#logs = open("logs_knn_random_forest.txt", "w")

def getAllFileNames(dir):
    files = []
    for filename in os.listdir(dir):
        files.append(filename)
    files.sort()
    return files

def getLabelOfDir(path, checkDir):
    dirs = getAllFileNames(path)
    counter = 0
    for dir in dirs:
        if dir == checkDir:
            return counter
        counter += 1
    return -1


def getListOfFeatures(path):
    f = open(path)
    res = []
    for line in f:
        line = line[1:len(line)-2]
        curList = [float(item) for item in line.split(',')]
        res.append(curList)
    flattened = np.array(res).flatten()
    result = []
    for i in range(0, len(flattened)):
        result.append(flattened[i])
    return result
    
    
#def getListOfFeatures(path):
 #   f = open(path)
  #  res = []
   # for line in f:
    #    line = line[1:len(line)-2]
    #    curList = [float(item) for item in line.split(',')]
     #   res.append(curList)
   # return res



def load_train_data_np(path):
    dirs = getAllFileNames(path)
    data = np.empty(shape=(10880000, 200), dtype=np.float16)
    index = 0
    for dir in dirs:
        print("reading " + dir + "...")
        files = getAllFileNames(path + "/" + dir)
        print("Len is " + str(len(files)))
        for file in files:
            if "_xxxx" not in file:  
                featureList = getListOfFeatures(path + "/" + dir + "/" + file)
                for i in range(0, len(featureList)):
                    data[index][i] = np.float16(featureList[i])
                    index += 1


    print(data.shape)



def load_train_data(path):
    dirs = getAllFileNames(path)
    data = []
    target = []
    label = 0
        
    for dir in dirs:
        print(dir)
        
    for dir in dirs:
        print("Reading " + dir + "...")
        files = getAllFileNames(path + "/" + dir)
        print("LEN is " + str(len(files)))
        for file in files:
            if "_xxxx" not in file:
                featureList = getListOfFeatures(path + "/" + dir + "/" + file)
                print("GOT FILE " + file + " shape is " + str(len(featureList)))
                data.append(featureList)
                target.append(label)
                #for i in range(0, len(featureList)):
                 #   data.append(featureList[i])
                  #  target.append(label)
        label += 1
        
    return np.array(data), np.array(target)


def label_test_data(path, percentage):
    dirs = getAllFileNames(path)
    for dir in dirs:
        print("Reading " + dir)
        files = getAllFileNames(path + "/" + dir)
        print("Got " + str(len(files)) + " files")
        counter = int(len(files) * (percentage / 100.0))
        print("counter is " + str(counter))
        for file in files:
            if counter <= 0:
                break
            os.rename(path + "/" + dir + "/" + file, path + "/" + dir + "/" + file[0:file.find('.')] + "_xxxx" + file[file.find('.'):])
            counter -= 1


def getMajorityLabel(y_pred):
    labels = defaultdict(int)
    for i in range(0, len(y_pred)):
        labels[y_pred[i]] += 1

    maxCounterVal = 0
    maxLabel = -1
    for key, value in labels.items():
        # print("label " + str(key) + " " + str(value))
        if value >= maxCounterVal:
            maxLabel = key
            maxCounterVal = value

    return maxLabel, maxCounterVal


def classifyRandomForest(label, pathToImageFeatures, clf):
    x_test = getListOfFeatures(pathToImageFeatures)
    xx_test = []
    xx_test.append(x_test)
    xx_test = np.array(xx_test)
    
    y_test = np.empty(len(xx_test))
    y_test.fill(label)
    
    
    y_pred = clf.predict(xx_test)

    majorLabel, counter = getMajorityLabel(y_pred)

    flag = "NO"
    if majorLabel == label:
        flag = "YES"

    accuracy = metrics.accuracy_score(y_test, y_pred)
    curLog = "SVM for " + pathToImageFeatures + " is " + str(accuracy) + " " + flag
    ##print(curLog)
    ##logs.write(curLog)
    ##logs.write(os.linesep)
    ##logs.flush()

    if majorLabel == label:
        return True

    return False


def classifyKNN(label, pathToImageFeatures, knn):
    x_test = getListOfFeatures(pathToImageFeatures)
    xx_test = []
    xx_test.append(x_test)
    xx_test = np.array(xx_test)
    #print("TEST SHAPE IS !")
    #print(xx_test.shape)
    
    y_test = np.empty(len(xx_test))
    y_test.fill(label)

    y_pred = knn.predict(xx_test)

    majorLabel, counter = getMajorityLabel(y_pred)
    # print("majorLabel is " + str(majorLabel) + " " + str(counter))
    accuracy = metrics.accuracy_score(y_test, y_pred)

    flag = "NO"
    if majorLabel == label:
        flag = "YES"

    curLog = "KNN for " + pathToImageFeatures + " is " + str(accuracy) + " " + flag
    ##print(curLog)
    ##logs.write(curLog)
    ##logs.write(os.linesep)
    ##logs.flush()

    if majorLabel == label:
        return True

    return False







def classifyForAllImages(clf, knn, path):
    hitCounter = 0
    totalCounter = 0

    dirs = getAllFileNames(path)

    
    for dir in dirs:
        label = getLabelOfDir(path, dir)
        files = getAllFileNames(path + "/" + dir)
        for file in files:
            pathToFile = path + "/" + dir + "/" + file
            if "_xxxx" in file:
                if classifyKNN(label, pathToFile, knn):
                    hitCounter += 1
                elif classifyRandomForest(label, pathToFile, clf):
                    hitCounter += 1
                totalCounter += 1

    return hitCounter, totalCounter


def classify_svm_knn(label, pathDir, k, kernel, leafSize, x_train, y_train):
    start = time.time()
    if label:
        print("Labelling data...")
        label_test_data(pathDir, 10)
        #exit()

    


    #print("X_TRAING SIZE " + str(len(x_train)))
    #print(x_train.shape)
    #print("Y_TRAING SIZE " + str(len(y_train)))
    #print(y_train.shape)
    
    #categories = getAllFileNames(pathDir)
    #filesPerCategory = len(getAllFileNames(pathDir + "/" + categories[0]))
    #f = open("results_knn_random_forest" + "_" + str(k) + "_" + str(len(categories)) + "_" + str(filesPerCategory) + ".txt", "w")

    print("GOT K " + str(k) + " AND LEAF SIZE " + str(leafSize))
    #print("Training KNN")
    knn = KNeighborsClassifier(n_neighbors=k)
    knn.fit(x_train, y_train)
    print("TIME SPENT KNN TRAINING IS " + str(time.time() - start) + "s")
    #f.write("TIME SPENT KNN TRAINING IS " + str(time.time() - start) + "s")
    #f.write(os.linesep)
    #f.flush()


    #print("Training RANDOM FOREST")
    #clf = svm.SVC(kernel=kernel)
    #clf = OneVsRestClassifier(SVC(kernel=kernel, probability=True, class_weight='balanced'))

    clf = RandomForestClassifier(min_samples_leaf=leafSize)
    clf.fit(x_train, y_train)
    #f.write("TIME SPENT FOR RANDOM FOREST TRAINING IS " + str(time.time() - start) + "s")
    print("TIME SPENT FOR RANDOM FOREST TRAINING IS " + str(time.time() - start) + "s")
    #f.write(os.linesep)
    #f.flush()



    hitCounter, totalCounter = classifyForAllImages(clf, knn, pathDir)

    #print(str(hitCounter) + " out of " + str(totalCounter))
    #f.write(str(hitCounter) + " out of " + str(totalCounter))
    #f.write(os.linesep)
    #f.flush()

    print("Total accuracy is " + str(float(hitCounter) / float(totalCounter)) + "-" * 50 + '\n')
    #f.write("Total accuracy is " + str(float(hitCounter) / float(totalCounter)))
    #f.write(os.linesep)
    #f.flush()

    #print("TIME SPENT IS " + str(time.time() - start) + "s")
    #f.write("TIME SPENT IS " + str(time.time() - start) + "s")
    #f.write(os.linesep)
    #f.flush()
    #f.close()
    totalAccuracy = float(hitCounter) / float(totalCounter)
    return totalAccuracy

#f = getListOfFeatures("data/4/4_gorilla_1266_xxxx.txt")
#p#rint(f)
#exit()
#classify_svm_knn(False, "image_net_embedded_sample_features_4096x100", 300, "rbf")
#acc = classify_svm_knn(False, "data", 250, "rbf", 20)


start = time.time()
max_k = 1
max_leaf = 1
maxAccuracy = 0.0

x_train, y_train = load_train_data("data")


for k in range(1, 300):
    for leaf in range(1, 100):
        acc = classify_svm_knn(False, "data", k, "rbf", leaf, x_train, y_train)
        print("MAXIMUM ACCURACY SO FAR " + str(maxAccuracy) + " k= " + str(max_k) + " leaf = " + str(max_leaf) + " " + "*" * 60)
        if acc >= maxAccuracy:
            max_k = k
            max_leaf = leaf
            maxAccuracy = acc


print("THE MAXIMUM ACC IS " + str(maxAccuracy))
print("MAX K is " + str(max_k))
print("MAX LEAF is " + str(max_leaf))
print("IN TOTAL TIME SPENT = " + str(time.time() - start) + "s")      







