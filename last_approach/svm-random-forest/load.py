import shutil
import os

def getAllFileNames(dir):
    files = []
    for filename in os.listdir(dir):
        files.append(filename)
    files.sort()
    return files
    

def getLabel(imgName):
    return imgName[0:imgName.find('_')]
    
files = getAllFileNames("prepared_all_data_")
for file in files:
    label = getLabel(file)
    shutil.copy("prepared_all_data_/" + file, "data/" + label + "/" + file)

