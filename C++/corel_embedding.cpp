#include <bits/stdc++.h>
#include <dirent.h>
using namespace std;

#define ll long long
#define ull unsigned long long
#define pb push_back
#define sz size()
#define sqr(x) ((x) * (x))
#define F first
#define S second
#define all(c) c.begin(), c.end()
#define rall(c) c.rbegin(), c.rend()
#define tr(container, it) \
 for(typeof(container.begin()) it = container.begin(); it != container.end(); it++)
#define present(container, element) (container.find(element) != container.end()) // set, map
#define cpresent(container, element) (find(all(container), element) != container.end())\\

const int N = 100;

namespace {
std::vector<std::string> GetDirectoryFiles(const std::string& dir) {
        std::vector<std::string> files;
        std::shared_ptr<DIR> directory_ptr(opendir(dir.c_str()), [](DIR* dir){ dir && closedir(dir); });
        struct dirent *dirent_ptr;
            if (!directory_ptr) {
            std::cout << "Error opening : " << std::strerror(errno) << " " << dir << std::endl;
            return files;
        }

        while ((dirent_ptr = readdir(directory_ptr.get())) != nullptr) {
            files.push_back(std::string(dirent_ptr->d_name));
        }
	sort(files.begin(), files.end());
        return files;
    }
}


vector<float> getValues(const string& path) {
    vector<float> v;
    string line, str, cur = "";
    ifstream myfile (path);

    if (myfile.is_open()) {
        while ( getline (myfile,line) ) {
            str = line;
        }
        myfile.close();
    } else cout << "Unable to open file" << endl;

    for (int i = 0; i < str.size(); i++) {
        if (str[i] == ' ') {
            v.pb(stof(cur));
            if (v.size() >= N) {
                return v;
            }
            cur = "";
        } else {
            cur += str[i];
        }
    }
    if (!cur.empty()) {
        v.pb(stof(cur));
    }
    return v;
}




vector<float> dijkstra(int start, const vector<float>& values) {
    vector<float> dist(N, DBL_MAX);
    dist[start] = 0;
    set<pair<double, int> > cur;
    cur.insert({dist[start], start});

    while (!cur.empty()) {
        auto curNode = cur.begin()->second;
        cur.erase(cur.begin());

        for (int to = 0; to < N; to++) {
            if (to != curNode) {
                float w = abs(values[to] - values[curNode]);
                if (dist[to] > dist[curNode] + w) {
                    cur.erase({dist[to], to});
                    dist[to] = dist[curNode] + w;
                    cur.insert({dist[to], to});
                }
            }
        }
    }
    return dist;
}

vector<vector<float> > getEmbeddings(const vector<float>& values) {
    vector<vector<float> > res;
    for (int node = 0; node < N; node++) {
        res.push_back(dijkstra(node, values));
    }
    return res;
}

void writeToAFile(const string& path, const vector<vector<float> >& embedding) {
    ofstream myfile;
    myfile.open (path);

    for (int i = 0; i < embedding.size(); i++) {
        myfile << "[";
        for (int j = 0; j < embedding[i].size(); j++) {
            if (j != embedding[i].size() - 1) {
                myfile << embedding[i][j] << ",";
            } else {
                myfile << embedding[i][j] << "]\n";
            }
        }
    }
    myfile.close();
}

int main(int argc, char const *argv[]) {
//    freopen("in", "r", stdin);
//    freopen("out", "w", stdout);
    auto dirs = GetDirectoryFiles("CorelDB_features");
    int counter = 0;

    for (const auto& dir : dirs) {
        auto files = GetDirectoryFiles("CorelDB_features/" + dir);
        for (const auto& file : files) {
            if (file.find(".txt") != string::npos) {
                cerr << "Adding " << dir << " " + file << " " << counter << endl;
                counter++;

                auto features = getValues("CorelDB_features/" + dir + "/" + file);
                auto allEmb = getEmbeddings(features);
                writeToAFile("CorelDB_embedded_features/" + dir + "/" + file, allEmb);

            }
        }
    }

    return 0;
}
