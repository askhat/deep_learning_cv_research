#include <bits/stdc++.h>
using namespace std;

#define ll long long
#define ull unsigned long long
#define pb push_back
#define sz size()
#define sqr(x) ((x) * (x))
#define F first
#define S second
#define all(c) c.begin(), c.end()
#define rall(c) c.rbegin(), c.rend()
#define tr(container, it) \
 for(typeof(container.begin()) it = container.begin(); it != container.end(); it++)
#define present(container, element) (container.find(element) != container.end()) // set, map
#define cpresent(container, element) (find(all(container), element) != container.end())\\

//the dimensionality of an embedding vectors
const int N = 1000;
//distance
double adj[N][N];

vector<float> getValues(const string& str, int n) {
    string cur = "";
    vector<float> v;
    for (int i = 0; i < str.size(); i++) {
        if (str[i] == ' ') {
            v.pb(stof(cur));
            if (v.size() >= n) {
                return v;
            }
            cur = "";
        } else {
            cur += str[i];
        }
    }
    if (!cur.empty()) {
        v.pb(stof(cur));
    }
    return v;
}

double getEuclidean(const vector<float>& v1, const vector<float>& v2) {
	double total = 0.0;
	for (int i = 0; i < v1.sz; i++) {
		total += sqr(v1[i] - v2[i]);
	}
	return sqrt(total);
}

void fillAdjMatrix(const vector<vector<float>>& nodes) {
	for (int i = 0; i < nodes.sz; i++) {
		for (int j = 0; j < nodes.sz; j++) {
			adj[i][j] = getEuclidean(nodes[i], nodes[j]);
		}
	}
}

void fillAdjMatrixChebyshev(const vector<float>& nodes) {
    for (int i = 0; i < nodes.size(); i++) {
        for (int j = 0; j < nodes.size(); j++) {
            adj[i][j] = abs(nodes[i] - nodes[j]);
        }
    }
}


vector<double> dijkstra(int start) {
	double dist[N];
	for (int i = 0; i < N; i++) dist[i] = DBL_MAX;


	dist[start] = 0;
	set<pair<double, int> > cur;
	cur.insert({dist[start], start});

	while (!cur.empty()) {
		auto node = cur.begin()->second;
		cur.erase(cur.begin());

		for (int to = 0; to < N; to++) {
			double w = adj[node][to];

			if (dist[to] > dist[node] + w) {
				cur.erase({dist[to], to});
				dist[to] = dist[node] + w;
				cur.insert({dist[to], to});
			}
		}
	}
	vector<double> res;
	for (int i = 0; i < N; i++) {
//		cout << start << " -> " << i << " = " << dist[i] << endl;
		res.pb(dist[i]);
	}
	return res;
}



void getEmbeddings() {
	for (int i = 0; i < N; i++) {
		auto cur = dijkstra(i);
		cout << i << " = { ";
		for (int j = 0; j < cur.sz; j++) {
			cout << cur[j] << " ";
		}
		cout << "}";
		cout << endl;
	}
}


int main(int argc, char const *argv[]) {
    freopen("in", "r", stdin);
   	freopen("out", "w", stdout);
    string str;
    getline(cin, str);
    auto v = getValues(str, 1000);
    cout << "Got " << v.size() << endl;
    for (const auto& el : v) {
        cout << el << endl;
    }













	// vector<vector<float> > nodes;
	// string str;
	// int cnt = 0;
    // while(getline(cin, str)) {
    // 	if (str.empty()) continue;
    // 	if (str.find("png") != string::npos) {
    //
	// 		if (cnt != 0) {
	// 			fillAdjMatrix(nodes);
	// 			getEmbeddings();
	// 		}
	// 		cout << str << endl;
    //
    // 		cnt++;
    // 		nodes.clear();
    // 	} else {
    // 		nodes.pb(getValues(str));
    // 	}
    // }
    // fillAdjMatrix(nodes);
    // getEmbeddings();



/*    fillAdjMatrix(nodes);
    for (int i = 0; i < nodes.size(); i++) {
    	for (int j = 0; j < nodes[i].sz; j++) {
    		cout << nodes[i][j] << " ";
    	}
    	cout << endl;
    }
	getEmbeddings();*/

    return 0;
}
