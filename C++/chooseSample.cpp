#include <bits/stdc++.h>
using namespace std;

#define ll long long
#define ull unsigned long long
#define pb push_back
#define sz size()
#define sqr(x) ((x) * (x))
#define F first
#define S seconds
#define all(c) c.begin(), c.end()
#define rall(c) c.rbegin(), c.rend()
#define tr(container, it) \
 for(typeof(container.begin()) it = container.begin(); it != container.end(); it++)
#define present(container, element) (container.find(element) != container.end()) // set, map
#define cpresent(container, element) (find(all(container), element) != container.end())\\


vector<string> getValues(const string& str) {
    string cur = "";
    vector<string> res;
    for (int i = 0; i < str.size(); i++) {
        if (str[i] == ' ') {
            res.push_back(cur);
            cur = "";
        } else {
            cur += str[i];
        }
    }
    if (!cur.empty()) {
        res.push_back(cur);
    }
    return res;
}

unordered_map<string, vector<pair<float, string> > > catImages;


vector<string> getTop(string img) {
    auto v = catImages[img];
    sort(v.rbegin(), v.rend());
    vector<string> res;
    for (int i = 0; i < min(3, (int)v.size()); i++) {
        res.push_back(v[i].second);
    }
    return res;
}


int main(int argc, char const *argv[]) {
    #ifndef ONLINE_JUDGE
    freopen("categories.txt", "r", stdin); freopen("out", "w", stdout);
    #endif
    string str;

    while(getline(cin, str)) {
        auto v = getValues(str);
        auto img = v[0], cat = v[1], acc = v[2];
        catImages[cat].push_back({stof(acc), img});
    }

    vector<pair<int, string> > v;
    for (const auto& el : catImages) {
        v.push_back({el.second.size(), el.first});
    }
    sort(v.rbegin(), v.rend());
    // for (const auto& el : v) {
    //     cout << el.first << " " << el.second << endl;
    // }

    vector<string> res;
    for (int i = 0; i < 700; i++) {
        auto toAdd = getTop(v[i].second);
        res.insert(res.end(), toAdd.begin(), toAdd.end());
    }


    for (const auto& img : res) {
        cout << img << endl;
    }















    return 0;
}
