#include <bits/stdc++.h>
#include <dirent.h>
#include <chrono>


using namespace std;
using namespace std::chrono;

#define ll long long
#define ull unsigned long long
#define pb push_back
#define sz size()
#define sqr(x) ((x) * (x))
#define F first
#define S second
#define all(c) c.begin(), c.end()
#define rall(c) c.rbegin(), c.rend()
#define tr(container, it) \
 for(typeof(container.begin()) it = container.begin(); it != container.end(); it++)
#define present(container, element) (container.find(element) != container.end()) // set, map
#define cpresent(container, element) (find(all(container), element) != container.end())\\

const int N = 4096;

vector<string> GetDirectoryFiles(const string& dir) {
    vector<string> files;
    shared_ptr<DIR> directory_ptr(opendir(dir.c_str()), [](DIR* dir){ dir && closedir(dir); });
    struct dirent *dirent_ptr;
        if (!directory_ptr) {
        cout << "Error opening : " << strerror(errno) << " " << dir << endl;
        return files;
    }

    while ((dirent_ptr = readdir(directory_ptr.get())) != nullptr) {
        files.push_back(string(dirent_ptr->d_name));
    }
    sort(files.begin(), files.end());
    return files;
}



vector<float> getValues(const string& path) {
    cout << "getting values from " << path << endl;
    vector<float> v;
    string line, str, cur = "";
    ifstream myfile (path);
    if (myfile.is_open()) {
        while (getline (myfile,line) ) {
            str = line;
        }
        myfile.close();
    } else cout << "Unable to open file" << endl;

    for (int i = 0; i < str.size(); i++) {
        if (str[i] == ' ') {
            v.pb(stof(cur));
            if (v.size() >= N) {
                return v;
            }
            cur = "";
        } else {
            cur += str[i];
        }
    }
    if (!cur.empty()) {
        v.pb(stof(cur));
    }
    return v;
}


void writeToAFile(string& path, const vector<vector<float> >& embedding) {
    cout << "writing to " << path << endl;
    ofstream myfile;
    myfile.open (path);
    for (int i = 0; i < embedding.size(); i++) {
        myfile << "[";
        for (int j = 0; j < embedding[i].size(); j++) {
            if (j != embedding[i].size() - 1) {
                myfile << embedding[i][j] << ",";
            } else {
                myfile << embedding[i][j] << "]\n";
            }
        }
    }
    myfile.close();
}




struct Node {
    Node* parent;
    int rank;
    double data;
    Node (int x) : rank(0), data(x), parent(this) {}
};

void createSet(int data, unordered_map<int, Node*>& getNode) {
    Node* temp = new Node(data);
    getNode[data] = temp;
}

Node* getParent(Node* node, unordered_map<int, Node*>& getNode) {
    Node* parent = node->parent;
    if (parent == node) {
        return parent;
    }
    node->parent = getParent(node->parent, getNode);
    return node->parent;
}

bool unionSets(int data1, int data2, unordered_map<int, Node*>& getNode) {
    Node* parent1 = getParent(getNode[data1], getNode), *parent2 = getParent(getNode[data2], getNode);
    if (parent1 == parent2) {
        return false;
    }
    if (parent1->rank >= parent2->rank) {
        parent2->parent = parent1;
        parent1->rank++;
    } else {
        parent1->parent = parent2;
    }
    return true;
}

int getRepresentative(int data, unordered_map<int, Node*>& getNode) {
    return getParent(getNode[data], getNode)->data;
}

vector<pair<double, pair<int,int>>> getEdges(const vector<float>& features) {
    vector<pair<double, pair<int, int>>> res;
    int n = features.size();
    for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
            double dist = abs(features[i] - features[j]);
            res.pb({dist, {i, j}});
        }
    }
    return res;
}

vector<pair<double, pair<int,int>>> getMSTEdges(vector<pair<double, pair<int,int>>>& edges, int n) {
    vector<pair<double, pair<int,int>>> res;
    unordered_map<int, Node*> getNode;
    for (int i = 0; i < n; i++) createSet(i, getNode);
    sort(begin(edges), end(edges));

    for (const auto& edge : edges) {
        int from = edge.S.F, to = edge.S.S;
        if (unionSets(from, to, getNode)) {
            res.pb(edge);
        }
    }
    return res;
}



vector<vector<pair<int, double>>> constructGraph(int n, const vector<pair<double, pair<int,int>>>& edges) {
    vector<vector<pair<int, double>>> graph(n, vector<pair<int, double>>());
    for (const auto& edge : edges) {
        int from = edge.S.F, to = edge.S.S;
        double dist = edge.F;
        graph[from].push_back({to, dist});
        graph[to].push_back({from, dist});
    }
    return graph;
}


void depthFirst(int node, double curDist, vector<pair<int, float>>& res,
    vector<bool>& used, const vector<vector<pair<int, double>>>& graph) {

    used[node] = true;
    res.push_back({node, curDist});
    for (const auto& node : graph[node]) {
        int to = node.first; double dist = node.second;
        if (!used[to]) {
            depthFirst(to, curDist + dist, res, used, graph);
        }
    }
}

vector<vector<float>> getDistancesMatrix(int n, const vector<pair<double, pair<int,int>>>& edges) {
    auto graph = constructGraph(n, edges);
    vector<vector<float>> res;

    for (int node = 0; node < n; node++) {
        vector<bool> used(n, false);
        vector<pair<int, float>> current;
        depthFirst(node, 0.0, current, used, graph);
        sort(begin(current), end(current));

        vector<float> temp;
        for (int j = 0; j < current.size(); j++) {
            temp.push_back(current[j].second);
        }
        res.push_back(temp);
    }
    return res;
}


vector<vector<float>> floydWarshal(int n, vector<pair<double, pair<int,int>>>& edges) {
    vector<vector<float>> dist(n, vector<float>(n, 2e9));
    for (int i = 0; i < n; i++) {
        dist[i][i] = 0;
    }
    for (const auto& edge : edges) {
        int from = edge.S.F, to = edge.S.S;
        double distance = edge.F;
        dist[from][to] = distance;
        dist[to][from] = distance;
    }
    for (int k = 0; k < n; k++) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                dist[i][j] = min(dist[i][j], dist[i][k] + dist[k][j]);
            }
        }
    }
    return dist;
}


vector<vector<float> > getEmbeddings(string& path) {
    auto features = getValues(path);
    auto edges = getEdges(features);
    //if we comment this part it's a clique
    // auto mstEdges = getMSTEdges(edges, features.sz);
    auto embedding = floydWarshal(features.sz, edges);
    return embedding;
}


void showEmbedding(const vector<vector<float>>& embedding) {
    for (const auto& emb : embedding) {
        for (const auto& val : emb) {
            cout << val << " ";
        }
        cout << endl;
    }
    cout << endl;
}
void solve(string& path, string& embeddedPath) {
     auto start = high_resolution_clock::now();

    int counter = 0;
    auto dirs = GetDirectoryFiles(path);
    for (auto& dir : dirs) {
        auto files = GetDirectoryFiles(path + "/" + dir);
        for (auto& file : files) {
            if (file.find(".txt") != string::npos) {
                cout << "Adding " << dir << " " + file << " " << counter << endl;
                counter++;
                string getPath = path + "/" + dir + "/" + file;
                auto allEmbeddings = getEmbeddings(getPath);
                cout << "got embeddings for " << file << endl;
                // showEmbedding(allEmbeddings);
                getPath = embeddedPath + "/" + dir + "/" + file;

                writeToAFile(getPath, allEmbeddings);
            }
        }
    }
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<milliseconds>(stop - start);
    cout << "Time taken by function: " << duration.count() << " microseconds" << endl;
}


int main(int argc, char const *argv[]) {
    string featuresPath = "image_net_transfer_learning_features_VGG", embeddedPath = "image_net_transfer_learning_features_VGG _clique_embedding";
    solve(featuresPath, embeddedPath);

    return 0;
}
