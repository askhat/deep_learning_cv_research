#include <bits/stdc++.h>
#include <dirent.h>
using namespace std;

#define ll long long
#define ull unsigned long long
#define pb push_back
#define sz size()
#define sqr(x) ((x) * (x))
#define F first
#define S second


//cutting size
const int N = 500;
string mainDir = "image_net_embedded_features", shrinkedDir = "image_net_shrinked_embedded_features";


namespace {
std::vector<std::string> GetDirectoryFiles(const std::string& dir) {
        std::vector<std::string> files;
        std::shared_ptr<DIR> directory_ptr(opendir(dir.c_str()), [](DIR* dir){ dir && closedir(dir); });
        struct dirent *dirent_ptr;
            if (!directory_ptr) {
            std::cout << "Error opening : " << std::strerror(errno) << " " << dir << std::endl;
            return files;
        }

        while ((dirent_ptr = readdir(directory_ptr.get())) != nullptr) {
            files.push_back(std::string(dirent_ptr->d_name));
        }
        sort(files.begin(), files.end());
        return files;
    }
}


vector<float> getVector(string& str) {
    vector<float> res;
    string cur = "";
    for (int i = 1; i < str.size(); i++) {
        if (str[i] == ',') {
            res.pb(stof(cur));
            if (res.size() >= N) {
                cur = "";
                return res;
            }
            cur = "";
        } else {
            cur += str[i];
        }
    }   
    if (!cur.empty()) {
        res.pb(stof(cur));
    }
    return res;
}


vector<vector<float> > getShrinkedData(const string& path) {
    vector<vector<float> > res;
    ifstream myfile(path);
    string str, line;

    if (myfile.is_open()) {
        while (getline (myfile,line) ) {
            str = line;
            auto curVector = getVector(str);
            res.pb(curVector);
            if (res.sz >= N) {
                return res;
            }
        }
        myfile.close();
    } else cout << "Unable to open file" << endl;
    return res;   
}



void writeToAFile(const string& path, const vector<vector<float> >& embedding) {
    cout << "writing to " << path << endl;
    ofstream myfile;
    myfile.open (path);

    for (int i = 0; i < embedding.size(); i++) { 
        myfile << "[";
        for (int j = 0; j < embedding[i].size(); j++) {
            if (j != embedding[i].size() - 1) {
                myfile << embedding[i][j] << ",";
            } else {
                myfile << embedding[i][j] << "]\n";
            }
        }
    }
    myfile.close();
}




int main(int argc, char const *argv[]) {
//    freopen("in", "r", stdin);
//    freopen("out", "w", stdout);
    
     
    int counter = 0;
    auto dirs = GetDirectoryFiles(mainDir);
    for (const auto& dir : dirs) {
        auto files = GetDirectoryFiles(mainDir + "/" + dir);
        for (const auto& file : files) {
            if (file.find(".txt") != string::npos) {
                cerr << "Adding " << dir << " " + file << " " << counter << endl;
                counter++;

                auto shrinkedData = getShrinkedData(mainDir + "/" + dir + "/" + file);
                cout << "got embeddings for " << file << endl;
                writeToAFile(shrinkedDir + "/" + dir + "/" + file, shrinkedData);

            }
        }
    }

    return 0;
}
