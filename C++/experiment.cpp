#include <dirent.h>
#include <cstring>
#include <iostream>
#include <vector>
#include <memory>
#include <cmath>
#include <unordered_map>
#include <algorithm>
#include <fstream>

using namespace std;
unordered_map<string, string> getLabel;

namespace {
std::vector<std::string> GetDirectoryFiles(const std::string& dir) {
        std::vector<std::string> files;
        std::shared_ptr<DIR> directory_ptr(opendir(dir.c_str()), [](DIR* dir){ dir && closedir(dir); });
        struct dirent *dirent_ptr;
            if (!directory_ptr) {
            std::cout << "Error opening : " << std::strerror(errno) << " " << dir << std::endl;
            return files;
        }

        while ((dirent_ptr = readdir(directory_ptr.get())) != nullptr) {
            files.push_back(std::string(dirent_ptr->d_name));
        }
        return files;
    }
}

vector<double> getValues(const string& path) {
    // cerr << "get values " << path << endl;
    vector<double> res;
    string line;
    ifstream myfile (path);
    string str;
    if (myfile.is_open()) {
        while ( getline (myfile,line) ) {
            str = line;
        }
        myfile.close();
    } else cout << "Unable to open file" << endl;

    string cur = "";
    for (int i = 0; i < str.size(); i++) {
        if (str[i] == ' ' && !cur.empty()) {
            res.push_back((double)stof(cur));
            cur = "";
        } else if (str[i] != ' ') {
            cur += str[i];
        }
    }

    return res;
}

double getEuclidean(const vector<double>& v1, const vector<double>& v2) {
    double res = 0.0;
    for (int i = 0; i < v1.size(); i++) {
        res += (v1[i] - v2[i]) * (v1[i] - v2[i]);
    }
    return sqrt(res);
}


bool isFromTheSameSet(const string& feature1, const string& feature2) {
    // cerr << "YAHOO " << feature1 << " " << feature2 << endl;
    return feature1.substr(0, feature1.find('_')) == feature2.substr(0, feature2.find('_'));
}



bool isFromTheSameSet_ImageNET(const string& feature1, const string& feature2) {
    string str1 = feature1.substr(0, feature1.size() - 4), str2 = feature2.substr(0, feature2.size() - 1);
    // cerr << endl;
    // cerr << "Label of " << str1 << " is " << getLabel[str1] << endl;
    // cerr << "Label of " << str2 << " is " << getLabel[str2] << endl;
    return getLabel[str1] == getLabel[str2];
}



bool checkFeature(const string& str, const string& path, int topResultCount) {
    auto files = GetDirectoryFiles(path);
    bool result = false;

    cerr << "start checking " << str.substr(0, str.size() - 4) << " in " << path << "..." << endl;
    vector<pair<double, string> > total;
    for (int i = 0; i < files.size(); i++) {
        if (files[i].find("JPEG") != string::npos && str.find("JPEG") != string::npos) {
            auto v2 = getValues(path + "/" + files[i]);
            auto v1 = getValues(path + "/" + str);

            if (v1.size() == v2.size() && v1.size() > 0) {
                // cout << "checking... " << files[i] << endl;
                double curDist = getEuclidean(v1, v2);
                total.push_back({curDist, files[i].substr(0, files[i].find("txt"))});
            }
        }
    }

    // cerr << "Total size is " << total.size() << endl;

    sort(total.begin(), total.end());
    for (int i = 0; i < min(topResultCount, (int)total.size()); i++) {
        cerr << str << "->" << total[i].second << " = " << total[i].first;
        if (isFromTheSameSet_ImageNET(str, total[i].second) && total[i].first != 0) {
            cerr << " YES " << endl;
            result = true;
        } else {
            cerr << endl;
        }
    }

    cerr << str << " ";
    if (result) {
        cerr << "YES" << endl;
        // exit(0);
    } else {
        cerr << "NO" << endl;
    }
    cerr << endl;
    // cerr << endl;
    return result;
}



void experimentResultOnTheDataset(const string& path, int topResultCount) {
    cout << "Starting experiment " << path << " " << topResultCount << endl;
    auto files = GetDirectoryFiles(path);
    if (files.size() == 0) {
        cerr << "The directory is empty";
        return;
    }

    int counter = 0;
    vector<string> matchedRes;
    for (int i = 0; i < files.size(); i++) {
        cerr << i + 1 << " out of " << files.size() << endl;
        if (files[i].find("JPEG") != string::npos) {
            cout << i + 1 << " out of " << files.size() << endl;
            cout << "counter " << counter << " out of " << files.size() << endl;
            if (checkFeature(files[i], path, topResultCount)) {
                counter++;
                matchedRes.push_back(files[i]);
            }
        }
    }
    cout << endl;
    cout << "FINALLY " << endl;
    cout << counter << " out of " << files.size() << endl;
    for (const auto& cur : matchedRes) {
        cout << cur << endl;
    }
}


void onlineQuery(const string& path, int topResultCount) {
    auto files = GetDirectoryFiles(path);

    string str;
    while(cin >> str) {
        if (str == "Quit") {
            break;
        }
        cout << "Write the png file " << endl;
        vector<pair<double, string> > total;
        for (int i = 0; i < files.size(); i++) {
            auto v2 = getValues(path + "/" + files[i]);
            auto v1 = getValues(path + "/" + str + ".txt");
            if (v1.size() == v2.size() && v1.size() > 0) {
                cout << "checking... " << files[i] << endl;
                double curDist = getEuclidean(v1, v2);
                total.push_back({curDist, files[i].substr(0, files[i].find("txt"))});
            }
        }
        sort(total.begin(), total.end());
        for (int i = 0; i < min(topResultCount, (int)total.size()); i++) {
            bool flag = false;
            if (isFromTheSameSet(str, total[i].second)) {
                flag = true;
            }
            cout << str << "->" << total[i].second << " = " << total[i].first;
            if (flag) {
                cout << " HEY " << endl;
            } else {
                cout << endl;
            }
        }
        if (checkFeature(str + ".txt", path, topResultCount)) {
            cout << "YES" << endl;
        } else {
            cout << "NO" << endl;
        }

        cout << "Write the name of an image " << endl;
    }
}


int main() {
    freopen("labels.txt", "r", stdin);
    freopen("imagenet_features_block5_pool_5000_20.out", "w", stdout);
    string img, label;
    while(cin >> img >> label) {
        getLabel[img] = label;
    }
    cerr << "starting imagenet_features_block5_pool_5000_20.out" << endl;
    experimentResultOnTheDataset("shrinked_imagenet_5000/imagenet_features_block5_pool", 20);

    return 0;
}
