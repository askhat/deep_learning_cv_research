// Title: OpenCV C++ Program to blur an image.
// Import the core header file
#include <opencv2/core/core.hpp>

// core - a compact module defining basic data structures,
// including the dense multi-dimensional array Mat and
// basic functions used by  all other modules.

// highgui - an easy-to-use interface to video
// capturing, image and video codecs, as well
// as simple UI capabilities.
#include <opencv2/highgui/highgui.hpp>

// imgproc - an image processing module that
// includes linear and non-linear image filtering,
// geometrical image transformations (resize, affine
// and perspective warping, generic table-based
// remapping) color space conversion, histograms,
// and so on.
#include <opencv2/imgproc/imgproc.hpp>

// The stdio.h header defines three variable types,
// several macros, and various functions for performing
// input and output.
#include <stdio.h>
#include <iostream>

// Namespace where all the C++ OpenCV functionality resides
using namespace cv;
using namespace std;

#include <dirent.h>
#include <cstring>
#include <iostream>
#include <vector>
#include <memory>
#include <cmath>
#include <unordered_map>
#include <algorithm>
#include <fstream>

using namespace std;

namespace {
std::vector<std::string> GetDirectoryFiles(const std::string& dir) {
        std::vector<std::string> files;
        std::shared_ptr<DIR> directory_ptr(opendir(dir.c_str()), [](DIR* dir){ dir && closedir(dir); });
        struct dirent *dirent_ptr;
            if (!directory_ptr) {
            std::cout << "Error opening : " << std::strerror(errno) << dir << std::endl;
            return files;
        }

        while ((dirent_ptr = readdir(directory_ptr.get())) != nullptr) {
            files.push_back(std::string(dirent_ptr->d_name));
        }
        return files;
    }
}


// We can also use 'namespace std' if need be.

int main() {
    auto files = GetDirectoryFiles("new_aloi");
    for (const auto& file : files) {
        if (file.find("png") != string::npos) {
            cout << file << endl;
            Mat image = imread("new_aloi/" + file, CV_LOAD_IMAGE_UNCHANGED);
            blur(image,image,Size(10,10));
            namedWindow( "bat", CV_WINDOW_AUTOSIZE );
            imwrite("all_blurred_images_aloi/" + file, image);
            cout << "Done" << endl;
        }
    }
    return 0;
}
