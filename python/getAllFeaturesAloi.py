# import os
# import glob
# from shutil import copyfile
#reshape(-1) for a directory
# source = os.listdir("aloi_ds/")
# destination = "new_aloi/"
# for file in source:
#     for filepath in glob.iglob('aloi_ds/' + file + '//' + '*.png'):
#         cur = filepath
#         fileName = cur[cur.rfind('/') + 1:]
#         copyfile(filepath, destination + fileName)
import os
import numpy as np
from keras.preprocessing import image
from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input
from keras.applications.vgg16 import VGG16
from keras.layers import *
from keras.models import Model
import numpy as np

from keras.preprocessing import image
from keras.applications.vgg16 import preprocess_input
from keras.applications import MobileNet
import numpy as np


# model = VGG16(weights='imagenet', include_top=False)

model = VGG16(weights='imagenet', include_top=False, input_shape = (224,224, 3))
def getFeaturesVGG(img_path):
    # model = VGG16(weights='imagenet', include_top=False)
    img = image.load_img(img_path, target_size=(224, 224))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    res = np.asarray(model.predict(x)[0])
    # print(res.shape)
    # res = res.reshape(-1)
    print(res.shape)
    return res

def getFeaturesVGGLayer(img_path):
    # print("got path " + str(img_path))
    block5_conv3 = model.get_layer("block5_pool").output
    f0 = Flatten()(block5_conv3)


    img = image.load_img(img_path, target_size=(224, 224))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)

    test_model = Model(inputs=model.input, outputs=f0)
    res = np.asarray(test_model.predict(x))
    # print(res.shape)
    return res

def findEuclideanDistance(features1, features2):
    cur1 = features1.reshape(-1)
    cur2 = features2.reshape(-1)
    cur = np.sum((cur1 - cur2) ** 2)
    return np.sqrt(cur)

def getAllFileNames(dir):
    files = []
    for filename in os.listdir(dir):
        files.append(filename)
    return files

def getCleanedFeatures(features):
    all = []
    for x in features:
        for y in x:
            cur = []
            for z in y:
                cur.append(z)
            all.append(cur)
    return all





allFiles = getAllFileNames("aloi_dataset")
allFiles.sort()

# features = getFeaturesVGG("new_aloi/251_c.png")
# for x in features:
#     for y in x:
#         counter = 0
#         for z in y:
#             counter += 1
#             print(z, end=" ")
#             f.write(str(z) + " ")
#         f.write(os.linesep)

print("Starting block pool 5")

totalCounter = 0

for i in range(0, len(allFiles)):
    counter = 0
    if allFiles[i].find(".png") != -1:
        f = open("aloi_features_block5_pool/" + allFiles[i] + ".txt", "w")

        print(allFiles[i] + " " + str(totalCounter))
        # counter += 1
        totalCounter += 1
        features = getFeaturesVGGLayer("aloi_dataset/" + allFiles[i])
        toWrite = features.reshape(-1)

        cur = []
        for x in toWrite:
            cur.append(x)

        cur.sort(reverse = True)
        for i in range(0, len(cur)):
            if (counter > 200000):
                # print("breaking..")
                break
            f.write(str(cur[i]) + " ")
            counter += 1


        f.write(os.linesep)
        f.close()



# counter = 0
# cur = ""
# for i in range(0, len(allFiles)):
#     features = getFeaturesVGG("new_aloi/" + allFiles[i])
#     cur += allFiles[i]
#     cur += ' '
#     # print(allFiles[i] + " ", end = "")
#     counter += 1
#     for x in np.nditer(features):
#         cur += str(x)
#         cur += ' '
#     print(str(counter) + " " + allFiles[i])
#     f.write(cur + os.linesep)
#     cur = ""
#
# f.close()