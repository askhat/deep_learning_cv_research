# import os
# import glob
# from shutil import copyfile
#reshape(-1) for a directory
# source = os.listdir("aloi_ds/")
# destination = "new_aloi/"
# for file in source:
#     for filepath in glob.iglob('aloi_ds/' + file + '//' + '*.png'):
#         cur = filepath
#         fileName = cur[cur.rfind('/') + 1:]
#         copyfile(filepath, destination + fileName)
import os
import numpy as np
from keras.preprocessing import image
from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input

model = VGG16(weights='imagenet', include_top=False)

def getFeaturesVGG(img_path):
    # model = VGG16(weights='imagenet', include_top=False)
    img = image.load_img(img_path, target_size=(224, 224))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    return model.predict(x)[0]

def findEuclideanDistance(features1, features2):
    cur1 = features1.reshape(-1)
    cur2 = features2.reshape(-1)
    cur = np.sum((cur1 - cur2) ** 2)
    return np.sqrt(cur)

def getAllFileNames(dir):
    files = []
    for filename in os.listdir(dir):
        files.append(filename)
    return files

def getCleanedFeatures(features):
    all = []
    for x in features:
        for y in x:
            cur = []
            for z in y:
                cur.append(z)
            all.append(cur)
    return all

f = open("aloi_features.txt", "w")
allFiles     = getAllFileNames("new_aloi")
allFiles.sort()

# features = getFeaturesVGG("new_aloi/251_c.png")
# for x in features:
#     for y in x:
#         counter = 0
#         for z in y:
#             counter += 1
#             print(z, end=" ")
#             f.write(str(z) + " ")
#         f.write(os.linesep)

counter = 0
for i in range(0, len(allFiles)):
    print(allFiles[i] + " " + str(counter))
    f.write(allFiles[i] + os.linesep)
    counter += 1
    features = getFeaturesVGG("new_aloi/" + allFiles[i])
    for x in features:
        for y in x:
            for z in y:
                f.write(str(z) + " ")
            f.write(os.linesep)
    f.write(os.linesep)





# counter = 0
# cur = ""
# for i in range(0, len(allFiles)):
#     features = getFeaturesVGG("new_aloi/" + allFiles[i])
#     cur += allFiles[i]
#     cur += ' '
#     # print(allFiles[i] + " ", end = "")
#     counter += 1
#     for x in np.nditer(features):
#         cur += str(x)
#         cur += ' '
#     print(str(counter) + " " + allFiles[i])
#     f.write(cur + os.linesep)
#     cur = ""
#
# f.close()
