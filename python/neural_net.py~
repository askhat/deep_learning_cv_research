from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import to_categorical
from keras.utils import np_utils
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import LabelEncoder

import os
import matplotlib.pyplot as plt
import pandas
import numpy as np
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from keras.layers import BatchNormalization
from keras import backend as K

K.tensorflow_backend._get_available_gpus()

def getAllFileNames(dir):
    files = []
    for filename in os.listdir(dir):
        files.append(filename)
    files.sort()
    return files

def getLabelOfDir(path, checkDir):
    dirs = getAllFileNames(path)
    counter = 0
    for dir in dirs:
        if dir == checkDir:
            return counter
        counter += 1
    return -1

def getListOfFeatures(path):
    f = open(path)
    res = []
    for line in f:
        line = line[1:len(line)-2]
        curList = [float(item) for item in line.split(',')]
        res.append(curList)
    return np.array(res).flatten()


def load_train_data(path):
    dirs = getAllFileNames(path)
    data = []
    target = []
    label = 0

    for dir in dirs:
        print("Reading " + dir + "...")
        files = getAllFileNames(path + "/" + dir)
        print("LEN is " + str(len(files)))
        for file in files:
            if ".txt" in file:
                featureList = getListOfFeatures(path + "/" + dir + "/" + file)
                data.append(featureList)
                target.append(label)
        label += 1
    return np.array(data), np.array(target)


def label_test_data(path, percentage):
    dirs = getAllFileNames(path)
    for dir in dirs:
        print("Reading " + dir)
        files = getAllFileNames(path + "/" + dir)
        print("Got " + str(len(files)) + " files")
        counter = int(len(files) * (percentage / 100.0))
        print("counter is " + str(counter))
        for file in files:
            if counter <= 0:
                break
            os.rename(path + "/" + dir + "/" + file, path + "/" + dir + "/" + file[0:file.find('.')] + "_test" + file[file.find('.'):])
            counter -= 1


import keras
# define baseline model
def baseline_model():
    # create model
    model = Sequential()
    model.add(Dense(100, input_dim=409600, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dense(100, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dense(70, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dense(50, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dense(9, activation='softmax'))
    # Compile model
    adam = keras.optimizers.Adam(lr = 0.01)
    model.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy'])
    return model


def modelFitValidation(path):
    xx_train, yy_train = load_train_data(path)
    yy_train = to_categorical(yy_train)
    # print(xx_train.shape[0])
    randomIndexes = np.arange(xx_train.shape[0])
    # print(randomIndexes)
    np.random.shuffle(randomIndexes)
    # print(randomIndexes)

    x_train = np.ndarray(shape=(xx_train.shape[0], xx_train.shape[1]), dtype=float)
    y_train = np.ndarray(shape=(yy_train.shape[0], yy_train.shape[1]), dtype=float)

    index = 0
    while index < len(randomIndexes):
        x_train[index] = xx_train[randomIndexes[index]]
        y_train[index] = yy_train[randomIndexes[index]]
        index += 1



    # print(x_train.shape)
    # print(y_train.shape)
    # print(y_train)
    # print(x_train)




    model = baseline_model()
    history = model.fit(x_train, y_train, validation_split=0.33, epochs=200, batch_size=40, verbose=1)
    print(history.history.keys())
    # summarize history for accuracy
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()

    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()


def modelFitWithKfoldCross(path):
    x_train, y_train = load_train_data(path)

    # k-fold cross validation
    estimator = KerasClassifier(build_fn=baseline_model, epochs=200, batch_size=20, verbose=1)
    kfold = KFold(n_splits=10, shuffle=True)
    results = cross_val_score(estimator, x_train, y_train, cv=kfold)
    print("Baseline: %.2f%% (%.2f%%)" % (results.mean() * 100, results.std() * 100))




# modelFitWithKfoldCross("image_net_embedded_features")
modelFitValidation("image_net_embedded_features")
