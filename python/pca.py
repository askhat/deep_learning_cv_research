from sklearn.datasets import load_iris
import numpy as NP
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

#source https://stackoverflow.com/questions/13224362/principal-component-analysis-pca-in-python
data = NP.array(NP.random.uniform(low=0.5, high=13.3, size=(100,100)))
# mean-centers and auto-scales the data
standardizedData = StandardScaler().fit_transform(data)


# Method 1: Have scikit-learn choose the minimum number of principal components
# such that at least x% (90% in example below) of the variance is retained.
pca = PCA(.90)
principalComponents = pca.fit_transform(X = standardizedData)

# To get how many principal components was chosen
print(pca.n_components_)
print(principalComponents)
print(principalComponents.shape)
