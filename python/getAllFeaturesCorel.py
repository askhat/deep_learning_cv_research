import os
from keras.applications.vgg16 import VGG16
from keras.layers import *
from keras.models import Model


from keras.preprocessing import image
from keras.applications.vgg16 import preprocess_input
import numpy as np



model = VGG16(weights='imagenet', include_top=False, input_shape = (224,224, 3))
def getFeaturesVGGLayer(img_path, layerName):
    block5_conv3 = model.get_layer(layerName).output
    f0 = Flatten()(block5_conv3)

    img = image.load_img(img_path, target_size=(224, 224))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)

    test_model = Model(inputs=model.input, outputs=f0)
    res = np.asarray(test_model.predict(x))
    res = res.reshape(-1)
    curList = []
    for feature in res:
        curList.append(feature)
    curList.sort(reverse=True)

    return curList

def getAllFileNames(dir):
    files = []
    for filename in os.listdir(dir):
        files.append(filename)
    return files

def writeToFile(path, featureList, topCount):
    f = open(path + ".txt", "w")
    counter = 0
    for i in range(0, len(featureList)):
        if counter > topCount:
            break
        f.write(str(featureList[i]) + " ")
        counter += 1
    f.write(os.linesep)
    f.close()




#configs
mainPath = "CorelDB"
layerName = "block5_pool"
featuresPath = "CorelDB_features"
topCount = 10000




dirs = getAllFileNames(mainPath)
for dir in dirs:
    os.mkdir(featuresPath + "/" + dir)
    images = getAllFileNames(mainPath + "/" + dir)
    print("Creating dir " + dir)
    counter = 0

    for img in images:
        if ".db" not in img:
            counter += 1
            features = getFeaturesVGGLayer(mainPath + "/" + dir + "/" + img, layerName)
            writeToFile(featuresPath + "/" + dir + "/" + img, features, topCount)
    print("DONE with " + dir + " " + str(counter) + " images extracted...")





