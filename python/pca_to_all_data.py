import numpy as np
import os

from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler


def getNPArrayFromTheEmbeddedFeatures(filePath):
    f = open(filePath)
    arr = np.zeros(shape=(100, 100))
    index_i = 0
    for str in f:
        curList = str[1:len(str)-2].split(',')
        for j in range(0, len(curList)):
            arr[index_i][j] = float(curList[j])
        index_i += 1
    return arr


def getPCA(data):
    standardizedData = StandardScaler().fit_transform(data)

    pca = PCA(n_components=50)
    principalComponents = pca.fit_transform(X=standardizedData)

    print("Got shape " + str(principalComponents.shape))
    return principalComponents



def getAllFileNames(dir):
    files = []
    for filename in os.listdir(dir):
        files.append(filename)
    files.sort()
    return files



def writeToAFile(path, data):
    f = open(path, "w")
    for i in range(0, len(data)):
        f.write("[")
        for j in range(0, len(data[i])):
            f.write('%s' % data[i][j])
            if j != len(data[i]) - 1:
                f.write(",")
            else:
                f.write("]")
                f.write(os.linesep)



dirs = getAllFileNames("CorelDB_embedded_features")
counter = 0
for dir in dirs:
    files = getAllFileNames("CorelDB_embedded_features/" + dir)
    for file in files:
        print("PCA for " + dir + "/" + file + " " + str(counter))
        counter += 1
        arr = getNPArrayFromTheEmbeddedFeatures("CorelDB_embedded_features/" + dir + "/" + file)
        pcaArr = getPCA(arr)
        writeToAFile("CorelDB_embedded_PCA_features_100x50/" + dir + "/" + file, pcaArr)






