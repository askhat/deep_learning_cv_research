import os

def getAllFileNames(dir):
    files = []
    for filename in os.listdir(dir):
        files.append(filename)
    files.sort()
    return files



def openFile(path):
    f = open(path, "r")
    res = f.read()
    return str(res)


def writeToFile(path, str):
    f = open(path, "w")
    f.write(str)
    f.write(os.linesep)
    f.close()



def getShrinked(curStr, shrinkSize):
    curList = []
    current = ""
    for i in range(0, len(curStr)):
        if curStr[i] == ' ':
            curList.append(current)
            current = ""
        else:
            current += curStr[i]

    if len(current) != 0:
        curList.append(current)

    print("Got " + str(len(curList)) + ' items')


    res = ""
    for i in range(0, min(shrinkSize, len(curList))):
        res += curList[i]
        res += " "
    return res


def shrinkFeatures(path):
    print("Starting for " + str(path))
    files = getAllFileNames(path)

    counter = 0
    for i in range(0, len(files)):
        print(files[i] + '...' + str(counter) + " out of " + str(len(files)))
        counter += 1
        cur = openFile(path + "/" + files[i])
        res = getShrinked(cur, 50000)
        writeToFile("shrinked_50000/" + path + "/" + files[i], res)




shrinkFeatures("aloi_features_block1_pool")
shrinkFeatures("aloi_features_block2_pool")
shrinkFeatures("aloi_features_block3_pool")
shrinkFeatures("aloi_features_block4_pool")
shrinkFeatures("aloi_features_block5_pool")


