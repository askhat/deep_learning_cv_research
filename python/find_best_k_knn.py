import os
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn import svm
from sklearn import metrics
from collections import defaultdict
import numpy as np

import time

from sklearn.neighbors import KNeighborsClassifier
from sklearn import metrics


logs = open("logs_knn.txt", "w")

def getAllFileNames(dir):
    files = []
    for filename in os.listdir(dir):
        files.append(filename)
    files.sort()
    return files

def getLabelOfDir(path, checkDir):
    dirs = getAllFileNames(path)
    counter = 0
    for dir in dirs:
        if dir == checkDir:
            return counter
        counter += 1
    return -1

def getListOfFeatures(path):
    f = open(path)
    res = []
    for line in f:
        line = line[1:len(line)-2]
        curList = [float(item) for item in line.split(',')]
        # print(type(curList))
        res.append(curList)
    # print(path + " LEN is " + str(len(res)) + ", width " + str(len(res[0])))
    return res


def load_train_data(path):
    dirs = getAllFileNames(path)
    data = []
    target = []
    label = 0

    for dir in dirs:
        print("Reading " + dir + "...")
        files = getAllFileNames(path + "/" + dir)
        print("LEN is " + str(len(files)))
        for file in files:
            if "_test" not in file:
                print("GOT " + file)
                featureList = getListOfFeatures(path + "/" + dir + "/" + file)
                for i in range(0, len(featureList)):
                    data.append(featureList[i])
                    target.append(label)
        label += 1
    return np.array(data), np.array(target)


def label_test_data(path, percentage):
    dirs = getAllFileNames(path)
    for dir in dirs:
        print("Reading " + dir)
        files = getAllFileNames(path + "/" + dir)
        print("Got " + str(len(files)) + " files")
        counter = int(len(files) * (percentage / 100.0))
        print("counter is " + str(counter))
        for file in files:
            if counter <= 0:
                break
            os.rename(path + "/" + dir + "/" + file, path + "/" + dir + "/" + file[0:file.find('.')] + "_test" + file[file.find('.'):])
            counter -= 1




def getMajorityLabel(y_pred):
    labels = defaultdict(int)
    for i in range(0, len(y_pred)):
        labels[y_pred[i]] += 1

    maxCounterVal = 0
    maxLabel = -1
    for key, value in labels.items():
        # print("label " + str(key) + " " + str(value))
        if value >= maxCounterVal:
            maxLabel = key
            maxCounterVal = value

    return maxLabel, maxCounterVal



def getAccuracyOfAnImageKNN(label, pathToImageFeatures, knn, k):
    # print("GOT " + pathToImageFeatures)
    # print("LABEL is " + str(label))


    x_test = getListOfFeatures(pathToImageFeatures)
    y_test = np.empty(len(x_test))
    y_test.fill(label)
    x_test = np.array(x_test)
    y_pred = knn.predict(x_test)


    # print("y_test is ")
    # print(y_test)
    # print("y_pred is ")
    # print(y_pred)
    # print("MAJORITY IS " + str(getMajorityLabel(y_pred)))

    majorLabel, counter = getMajorityLabel(y_pred)
    # print("majorLabel is " + str(majorLabel) + " " + str(counter))
    accuracy = metrics.accuracy_score(y_test, y_pred)

    flag = "NO"
    if majorLabel == label:
        flag = "YES"

    curLog = "Accuracy of KNN for " + pathToImageFeatures + " with k of " + str(k) + " is " + str(accuracy) + " " + flag
    print(curLog)
    logs.write(curLog)
    logs.write(os.linesep)

    if majorLabel == label:
        return True
    return False

    # Model Accuracy: how often is the classifier correct?

    # accuracy = metrics.accuracy_score(y_test, y_pred)


def testForAllImages(knn, path, k):
    dirs = getAllFileNames(path)
    totalCounter = 0
    hitCounter = 0
    for dir in dirs:
        label = getLabelOfDir(path, dir)
        files = getAllFileNames(path + "/" + dir)
        for file in files:
            if "_test" in file:
                if getAccuracyOfAnImageKNN(label, path + "/" + dir + "/" + file, knn, k):
                    hitCounter += 1
                totalCounter += 1

    accuracyOfKNN = float(float(hitCounter) / float(totalCounter))
    print(str(hitCounter) + " out of " + str(totalCounter))
    print(accuracyOfKNN)
    return accuracyOfKNN


def load_test_data(path):
    dirs = getAllFileNames(path)
    data = []
    target = []
    label = 0

    for dir in dirs:
        print("Reading " + dir + "...")
        files = getAllFileNames(path + "/" + dir)
        print("LEN is " + str(len(files)))
        for file in files:
            if "_test" in file:
                print("GOT " + file)
                featureList = getListOfFeatures(path + "/" + dir + "/" + file)
                for i in range(0, len(featureList)):
                    data.append(featureList[i])
                    target.append(label)
        label += 1
    return np.array(data), np.array(target)




# label_test_data("svm_test", 20)
x_train, y_train = load_train_data("svm_test")





f = open("knn_results.txt", "w")

maxAccuracy = 0
bestKValue = 1

start = time.time()


k_range = range(1, 20, 1)
scores = {}
scores_list = []


for k in k_range:
    print("Training for " + str(k))
    logs.write("Training for " + str(k) + " " + "*"*50)
    logs.write(os.linesep)

    knn = KNeighborsClassifier(n_neighbors=k)
    knn.fit(x_train, y_train)
    acc = testForAllImages(knn, "svm_test", k)
    # f.write("Test for " + str(k))
    # f.write(os.linesep)
    if acc >= maxAccuracy:
        maxAccuracy = acc
        bestKValue = k

    scores[k] = acc
    scores_list.append(acc)

    f.write("Accuracy for K:" + str(k) + " = " + str(acc))
    f.write(os.linesep)

    logs.write("Accuracy for K:" + str(k) + " = " + str(acc))
    logs.write(os.linesep)


f.write(os.linesep)
f.write("max accuracy is " + str(maxAccuracy) + " with k of " + str(bestKValue))
f.write(os.linesep)
f.write("Time spent is " + str(time.time() - start))

print("Files closed.")
logs.close()
f.close()


import matplotlib.pyplot as plt
plt.plot(k_range, scores_list)
plt.xlabel("Value of K for knn")
plt.ylabel("Testing accuracy")

plt.show()






# f = open("knn_results", "w")
# k_range = range(1, 1000)
# scores = {}
# scores_list = []
#
# for k in k_range:
#     knn = KNeighborsClassifier(n_neighbors=k)
#     knn.fit(x_train, y_train)
#
#     y_pred = knn.predict(x_test)
#     scores[k] = metrics.accuracy_score(y_test, y_pred)
#     scores_list.append(metrics.accuracy_score(y_test, y_pred))
#
#
#

