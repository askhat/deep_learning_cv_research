import os
import numpy as np
import keras
import shutil

from keras.models import Sequential
from keras.layers import BatchNormalization
from keras.layers import LeakyReLU
from keras.layers import Dropout
from my_classes import DataGenerator
from keras.layers import Dense
from keras import backend as K


params = {'dim': 90000,
          'batch_size': 64,
          'n_classes': 2,
          'shuffle': True}

#Datasets
partition = dict()
partition['train'] = list()
partition['validation'] = list()
labels = {}



os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
K.tensorflow_backend._get_available_gpus()

def GetListOfFeatures(path):
    f = open(path)
    res = []
    for line in f:
        line = line[1:len(line) - 2]
        curList = [float(item) for item in line.split(',')]
        res.append(curList)
    return np.array(res).flatten()


def getAllDirsAndFiles(dir):
    files = []
    for filename in os.listdir(dir):
        files.append(filename)
    return files


def getAllFileNames(dir):
    files = []
    for filename in os.listdir(dir):
        if ".txt" in filename:
            files.append(filename)
    return files


def renameFiles(path, label, labelName):
    files = getAllFileNames(path)
    counter = 1
    for file in files:
        if ".txt" in file:
            print("Renaming " + file)
            os.rename(path + "/" + file, path + "/" + str(label) + "_" + labelName + "_" + str(counter) + ".txt")
            counter += 1



def prepareData(path):
    dirs = getAllDirsAndFiles(path)
    print(dirs)
    label = 0
    for dir in dirs:
        renameFiles(path + "/" + dir, label, dir)

    print("RENAMED!!!")
    for dir in dirs:
        files = getAllFileNames(path + "/" + dir)
        for file in files:
            filePath = path + "/" + dir + "/" + file
            shutil.copy(filePath, "data/" + file)
            print(filePath + " moved!")




def getLabel(imgName):
    return int(imgName[0:imgName.find('_')])

def getPartitionAndLabels(path, divide):
    files = getAllFileNames(path)
    files.sort()
    index = 0

    while index < len(files):
        label = getLabel(files[index])
        imagesOfOneClass = []
        while index < len(files) and getLabel(files[index]) == label:
            imagesOfOneClass.append(files[index])
            index += 1


        #filling train
        trainSize = int(divide * len(imagesOfOneClass))
        for i in range(0, trainSize):
            partition['train'].append(imagesOfOneClass[i])

        #filling validation
        for i in range(trainSize, len(imagesOfOneClass)):
            partition['validation'].append((imagesOfOneClass[i]))

        #filling labels
        for i in range(0, len(imagesOfOneClass)):
            labels[imagesOfOneClass[i]] = label


def baseline_model():
    # create model

    model = Sequential()
    model.add(Dense(4, input_dim=90000, activation='relu'))
    model.add(BatchNormalization())

    model.add(Dense(90))
    model.add(LeakyReLU(alpha=0.05))
    model.add(Dropout(0.20))
    model.add(BatchNormalization())

    model.add(Dense(40))
    model.add(LeakyReLU(alpha=0.05))
    model.add(Dropout(0.20))
    model.add(BatchNormalization())

    model.add(Dense(2, activation='sigmoid'))

    # Compile model
    adam = keras.optimizers.Adam(lr=0.005)
    model.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy'])
    return model


def getListOfFeatures(path):
    f = open(path)
    res = []
    for line in f:
        line = line[1:len(line)-2]
        curList = [float(item) for item in line.split(',')]
        res.append(curList)
    return np.array(res).flatten()




prepareData("all_data")
getPartitionAndLabels("data", 0.8)


model = baseline_model()
training_generator = DataGenerator(partition['train'], labels, **params)
validation_generator = DataGenerator(partition['validation'], labels, **params)




history = model.fit_generator(generator=training_generator,
                    validation_data=validation_generator,
                    use_multiprocessing=True,
                    workers=6, epochs=10, verbose=1)

# evaluate the model
_, train_acc = model.evaluate(x_train, y_train, verbose=0)
_, test_acc = model.evaluate(x_test, y_test, verbose=0)
print('Train: %.3f, Test: %.3f' % (train_acc, test_acc))



print(history.history.keys())
# summarize history for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy, data_split=(80,20); architecture=(100,100,70,40,5); bs=64, adam=0.005')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss, data_split=(80,20); architecture=(100,100,70,40,5); bs=64, adam=0.005')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()



