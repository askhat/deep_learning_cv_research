import os

import math
from keras.layers import *
from keras.models import Model
from keras.preprocessing import image
from keras.applications import vgg16
from keras.applications.vgg16 import preprocess_input

import os
import numpy as np

def getFeatures(img_path):
    f = open(img_path).read()
    # print(f)
    features = [float(n) for n in f.split()]
    # features = [float(item) for item in f.split(' ')]
    # print("GOT LEN " + str(len(features)))
    return features

def getPrefix(features):
    cnt = 0
    for i in range(0, len(features)):
        if features[i] == 0:
            return cnt
            break
        cnt += 1
    return cnt

def getAllFileNames(dir):
    files = []
    for filename in os.listdir(dir):
        files.append(filename)
    files.sort()
    return files


def writeToFile(path, featureList):
    f = open(path + ".txt", "w")
    for i in range(0, len(featureList)):
        f.write(str(featureList[i]) + " ")
    f.write(os.linesep)
    f.close()


# configs
mainPath = "image_net_big_features_sample_4096"
dirs = getAllFileNames(mainPath)
maxPrefix = -1
counter = 0
for dir in dirs:
    images = getAllFileNames(mainPath + "/" + dir)
    for img in images:
        counter += 1
        features = getFeatures(mainPath + "/" + dir + "/" + img)
        prefix = getPrefix(features)
        maxPrefix = max(prefix, maxPrefix)
        print("prefix is " + str(prefix))
        # print("Feature size is " + str(len(features)) + " for " + img)
        # writeToFile(featuresPath + "/" + dir + "/" + img, features)
    # print("DONE with " + dir + " " + str(counter) + " images extracted...")
print("IN TOTAL " + str(counter) + " images")
print("MAX PREFIX IS " + str(maxPrefix))




