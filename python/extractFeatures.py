from keras.layers import *
from keras.models import Model
from keras.preprocessing import image
from keras.applications import vgg16
from keras.applications.vgg16 import preprocess_input


import os
import numpy as np


def get4096featuresVGG(img_path):
    img = image.load_img(img_path, target_size=(224, 224))
    model = vgg16.VGG16(weights='imagenet', include_top=True)
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    features = model.predict(x)
    model_extractfeatures = Model(input=model.input, output=model.get_layer('fc2').output)
    fc2_features = model_extractfeatures.predict(x)
    fc2_features = fc2_features.reshape((4096,1))


    featuresList = []
    for cur in fc2_features:
        featuresList.append(cur[0])
    featuresList.sort(reverse=True)


    f = open("fct2.txt", "w")
    for cur in featuresList:
        f.write(str(cur))
        f.write(os.linesep)

    return featuresList




def get25088featuresVGG(img_path):

    vgg_model = vgg16.VGG16(weights='imagenet', include_top=False, input_shape = (224,224, 3))
    block5_conv3 = vgg_model.get_layer("block5_pool").output
    f0 = Flatten()(block5_conv3)

    img = image.load_img(img_path, target_size=(224, 224))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)

    test_model = Model(inputs=vgg_model.input, outputs=f0)
    res = np.asarray(test_model.predict(x))

    print(res.shape)
    print(res)

get4096featuresVGG("elephant.jpg")
