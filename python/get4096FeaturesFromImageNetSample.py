import os
from keras.layers import *
from keras.models import Model
from keras.preprocessing import image
from keras.applications import vgg16
from keras.applications.vgg16 import preprocess_input


import os
import numpy as np



model = vgg16.VGG16(weights='imagenet', include_top=True)

def get4096featuresVGG(img_path):
    img = image.load_img(img_path, target_size=(224, 224))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    features = model.predict(x)
    model_extractfeatures = Model(input=model.input, output=model.get_layer('fc2').output)
    fc2_features = model_extractfeatures.predict(x)
    fc2_features = fc2_features.reshape((4096,1))


    featuresList = []
    for cur in fc2_features:
        featuresList.append(cur[0])
    featuresList.sort(reverse=True)

    return featuresList


def getAllFileNames(dir):
    files = []
    for filename in os.listdir(dir):
        files.append(filename)
    return files

def writeToFile(path, featureList):
    f = open(path + ".txt", "w")
    for i in range(0, len(featureList)):
        f.write(str(featureList[i]) + " ")
    f.write(os.linesep)
    f.close()




#configs
mainPath = "image_net_sample"
featuresPath = "image_net_sample_features"


dirs = getAllFileNames(mainPath)
for dir in dirs:
    os.mkdir(featuresPath + "/" + dir)
    images = getAllFileNames(mainPath + "/" + dir)
    print("Creating dir " + dir)
    counter = 0
    
    for img in images:
        counter += 1
        features = get4096featuresVGG(mainPath + "/" + dir + "/" + img)
        print("Feature size is " + str(len(features)) + " for " + img)
        writeToFile(featuresPath + "/" + dir + "/" + img, features)
    print("DONE with " + dir + " " + str(counter) + " images extracted...")





