#include <bits/stdc++.h>
#include <dirent.h>
using namespace std;

#define ll long long
#define ull unsigned long long
#define pb push_back
#define sz size()
#define sqr(x) ((x) * (x))
#define F first
#define S second

namespace {
std::vector<std::string> GetDirectoryFiles(const std::string& dir) {
        std::vector<std::string> files;
        std::shared_ptr<DIR> directory_ptr(opendir(dir.c_str()), [](DIR* dir){ dir && closedir(dir); });
        struct dirent *dirent_ptr;
            if (!directory_ptr) {
            std::cout << "Error opening : " << std::strerror(errno) << " " << dir << std::endl;
            return files;
        }

        while ((dirent_ptr = readdir(directory_ptr.get())) != nullptr) {
            files.push_back(std::string(dirent_ptr->d_name));
        }
        sort(files.begin(), files.end());
        return files;
    }
}

vector<float> getVector(string& str) {
    vector<float> res;
    string cur = "";
    for (int i = 1; i < str.size(); i++) {
        if (str[i] == ',') {
            res.pb(stof(cur));
            cur = "";
        } else {
            cur += str[i];
        }
    }
    if (!cur.empty()) {
        res.pb(stof(cur));
    }
    return res;
}

vector<float> getData(const string& path) {
    vector<float> res;
    ifstream myfile(path);
    string str, line;

    if (myfile.is_open()) {
        while (getline (myfile,line) ) {
            str = line;
            auto curVector = getVector(str);
            res.insert(res.end(), curVector.begin(), curVector.end());
        }
        myfile.close();
    } else cout << "Unable to open file" << endl;

    // cout << "GOT dimension " << res.size() << endl;
    return res;
}

vector<string> readFile(string fileName) {
    vector<string> res;
    ifstream file(fileName);
    string str;
    while (getline(file, str)) {
        res.pb(str);
    }
    return res;
}

float getEuclidean(vector<float>& v1, vector<float>& v2) {
    float sum = 0;
    for (int i = 0; i < v1.size(); i++) {
        sum += (v1[i] - v2[i]) * (v1[i] - v2[i]);
    }
    return sqrt(sum);
}

void saveToFile(const vector<float>& v, const string& img, const string& dir) {
    ofstream outfile(dir + "/" + img);
    outfile << "[";
    for (int i = 0; i < v.size(); i++) {
        outfile << v[i];
        if (i != v.size() - 1) {
            outfile << ",";
        }
    }
    outfile << "]";
}
int counter = 0;
void computeAndSave(const string& img, vector<string>& queries) {
    cout << "Starting " << img << "... " << counter << endl;
    counter++;
    auto data = getData("data/" + img);
    vector<float> res;
    for (int i = 0; i < queries.size(); i++) {
        auto features = getData("data/" + queries[i]);
        res.push_back(getEuclidean(data, features));
    }
    //save it
    saveToFile(res, img, "prepared_data");
    // counter++;
    // cout << "Finished " << counter << endl;
}


void saveFile(vector<string>& v, int index, int start, int end) {
    ofstream outfile("rest" + to_string(index) + ".txt");
    for (int i = start; i < min(end, (int)v.size()); i++) {
        outfile << v[i] << endl;
    }
}



string fileName = "rest.txt";

int main(int argc, char const *argv[]) {
//    freopen("in", "r", stdin);
//    freopen("out", "w", stdout);

    ///divide it into n parts
    /*int n = 10, cur = 0;
    auto rest = readFile("rest.txt");
    int step = rest.size() / n;
 
    for (int i = 0; i < n; i++) {
        saveFile(rest, i, cur, cur + step);
        cur += step;
    }   
    return 0;*/
    auto queries = readFile("queries.txt");
    auto rest = readFile(fileName);
    cout << "GOT " << fileName << endl;

    for (const auto& img : rest) {
        computeAndSave(img, queries);
    }
    cout << "Finished all! " << endl;
    
    
    
    return 0;
}
