import numpy as np
import keras
import math

#util functions
def getListOfFeatures(path):
    f = open(path)
    res = []
    for line in f:
        line = line[1:len(line)-2]
        curList = [float(item) for item in line.split(',')]
        res.append(curList)
    return np.array(res).flatten()

def getEuclidean(features1, features2):
    if len(features1) != len(features2):
        print("ERROR! Features do not have the same length")
        return
    sum = 0
    for i in range(0, len(features1)):
        sum += (features1[i] - features2[i]) * (features1[i] - features2[i])
    return math.sqrt(sum)

def getDistVectorForImageFromQueries(allQueries, ID):
    distVector = np.empty(len(allQueries))
    currentFeatures = getListOfFeatures("data/" + ID)
    for index in range(0, len(allQueries)):
        distVector[index] = getEuclidean(currentFeatures, getListOfFeatures("data/" + allQueries[index]))
    return distVector


class DataGenerator(keras.utils.Sequence):
    'Generates data for Keras'
    def __init__(self, list_IDs, labels, batch_size=32, dim=(32,32,32), n_channels=1,
                 n_classes=10, shuffle=True, queries=[]):
        'Initialization'
        self.dim = dim
        self.batch_size = batch_size
        self.labels = labels
        self.list_IDs = list_IDs
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.shuffle = shuffle
        self.queries = queries
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.list_IDs) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        X, y = self.__data_generation(list_IDs_temp)

        return X, y


    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        # Initialization
        # X = np.empty((self.batch_size, self.dim, self.n_channels))
        X = np.ndarray(shape=(self.batch_size, self.dim), dtype=float)
        y = np.empty((self.batch_size), dtype=int)

        # Generate data
        for i, ID in enumerate(list_IDs_temp):
            # Store sample
            X[i,] = getListOfFeatures("prepared_data/" + ID)
            # X[i,] = getDistVectorForImageFromQueries(self.queries, ID)
            # Store class
            y[i] = self.labels[ID]

        return X, keras.utils.to_categorical(y, num_classes=self.n_classes)
