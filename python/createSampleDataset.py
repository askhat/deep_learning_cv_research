


import shutil
import os





def getImages(path):
    with open(path, "r") as ins:
        array = []
        for line in ins:
            array.append(line)
    return array


def createDirsAndCopy(imgs, fromDirPath, copyDirPath):
    for img in imgs:
        cur = img.split()
        if not os.path.exists(copyDirPath + "/" + cur[1]):
            os.makedirs(copyDirPath + "/" + cur[1])
        shutil.copy(fromDirPath + "/" + cur[0], copyDirPath + "/" + cur[1] + "/" + cur[0])

imgs = getImages("sample.txt")
createDirsAndCopy(imgs, "ILSVRC2012_img_test", "image_net_sample")

