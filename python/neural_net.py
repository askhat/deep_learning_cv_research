from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import to_categorical
from keras.utils import np_utils
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import LabelEncoder
from keras.layers import BatchNormalization
from keras import backend as K
from keras.layers import LeakyReLU
from keras.layers import Dropout




import keras
import os
import random
import matplotlib.pyplot as plt
import pandas
import numpy as np

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


K.tensorflow_backend._get_available_gpus()

def getAllFileNames(dir):
    files = []
    for filename in os.listdir(dir):
        files.append(filename)
    files.sort()
    return files

def getListOfFeatures(path):
    f = open(path)
    res = []
    for line in f:
        line = line[1:len(line)-2]
        curList = [float(item) for item in line.split(',')]
        res.append(curList)
    return np.array(res).flatten()


def shuffle_and_make_categorical(x, y):
    y = to_categorical(y)
    print("GOT IN SHUFFLE")
    print(x.shape)
    print(y.shape)
    randomIndexes = np.arange(x.shape[0])
    np.random.shuffle(randomIndexes)
        
    x_res = np.ndarray(shape=(x.shape[0], x.shape[1]), dtype=float)
    y_res = np.ndarray(shape=(y.shape[0], y.shape[1]), dtype=float)

    index = 0
    while index < len(randomIndexes):
        x_res[index] = x[randomIndexes[index]]
        y_res[index] = y[randomIndexes[index]]
        index += 1
    return x_res, y_res

def load_train_data_with_proper_division(path, train_data_portion, toShuffle):
    dirs = getAllFileNames(path)
    
    x_train = []
    y_train = []
    
    x_test = []
    y_test = []
    
    label = 0    
    
    for dir in dirs:
        print("Reading " + dir + " ... ") 
        files = getAllFileNames(path + "/" + dir)
        if toShuffle:
            random.shuffle(files)
            
        print("LEN is " + str(len(files)))
        trainSize = int(len(files) * train_data_portion)
        testSize = int(len(files) - trainSize)
        
        print("trainSize, testSize " + str(trainSize) + " " + str(testSize))

        for i in range(0, trainSize):
            fileName = files[i]
            if ".txt" in fileName:
                x_train.append(getListOfFeatures(path + "/" + dir + "/" + fileName))
                y_train.append(label)

        for j in range(trainSize, len(files)):
            fileName = files[j]                    
            if ".txt" in fileName:
                x_test.append(getListOfFeatures(path + "/" + dir + "/" + fileName))
                y_test.append(label)
        label += 1
    
    x_train = np.array(x_train)
    y_train = np.array(y_train)
    
    x_test = np.array(x_test)
    y_test = np.array(y_test)

    x_train, y_train = shuffle_and_make_categorical(x_train, y_train)
    x_test, y_test = shuffle_and_make_categorical(x_test, y_test)
    return x_train, y_train, x_test, y_test         
    
    
    
def load_train_data(path):
    dirs = getAllFileNames(path)
    data = []
    target = []
    label = 0

    for dir in dirs:
        print("Reading " + dir + "...")
        files = getAllFileNames(path + "/" + dir)
        print("LEN is " + str(len(files)))
        for file in files:
            if ".txt" in file:
 #               print("Reading " + file)
                featureList = getListOfFeatures(path + "/" + dir + "/" + file)
                data.append(featureList)
                target.append(label)
        label += 1
    return np.array(data), np.array(target)


def label_test_data(path, percentage):
    dirs = getAllFileNames(path)
    for dir in dirs:
#        print("Reading " + dir)
        files = getAllFileNames(path + "/" + dir)
        print("Got " + str(len(files)) + " files")
        counter = int(len(files) * (percentage / 100.0))
        print("counter is " + str(counter))
        for file in files:
            if counter <= 0:
                break
            os.rename(path + "/" + dir + "/" + file, path + "/" + dir + "/" + file[0:file.find('.')] + "_test" + file[file.find('.'):])
            counter -= 1



def baseline_model():
    # create model
    model = Sequential()
    model.add(Dense(100, input_dim=206500))
    model.add(LeakyReLU(alpha=0.2))
    model.add(BatchNormalization())
    model.add(Dense(50))
    model.add(LeakyReLU(alpha=0.2))
    model.add(BatchNormalization())
    model.add(Dense(2, activation='softmax'))
    # Compile model
    adam = keras.optimizers.Adam(lr = 0.005)
    model.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy'])
    return model

def modelFitValidation(path):

    xx_train, yy_train, x_test, y_test = load_train_data_with_proper_division(path, 0.9, True)
    
    print("GOT TRAIN TOTAL SHAPE IS")
    print(xx_train.shape)
    print(yy_train.shape)

    print("GOT TEST TOTAL SHAPE IS")
    print(x_test.shape)
    print(y_test.shape)
    
   
    model = baseline_model()
    history = model.fit(xx_train, yy_train, validation_data=(x_test, y_test), epochs=10, batch_size=64, verbose=1)
   
   
    print(history.history.keys())
    # summarize history for accuracy
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy (100,90,70,50,5) bs = 64, adam=0.005')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()

    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss (100,90,70,50,5) bs = 64, adam=0.005')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()
    
    
    

def modelFitWithKfoldCross(path):
    x_train, y_train = load_train_data(path)
    # k-fold cross validation
    estimator = KerasClassifier(build_fn=baseline_model, epochs=200, batch_size=40, verbose=1)
    kfold = KFold(n_splits=10, shuffle=True)
    results = cross_val_score(estimator, x_train, y_train, cv=kfold)
    print("Baseline: %.2f%% (%.2f%%)" % (results.mean() * 100, results.std() * 100))




modelFitValidation("test")
#modelFitValidation("rest_ready_dataset")



# modelFitWithKfoldCross("image_net_embedded_features")


